#include <bits/stdc++.h>
using namespace std;

const int MAX = 1e7;

struct edge {
	int id, a, b;
	bool directed;
};

int getTo(const edge & e, int u) {
	if (!e.directed) return e.a == u ? e.b : e.a;
	return e.b;
}

bool isUsable(const edge & e, int u) {
	return !e.directed || e.a == u;
}

// orient edge u -> v
void orient(edge & e, int u, int v) {
	e.a = u;
	e.b = v;
	e.directed = true;
}

int N, M, num[MAX+5], DFI[MAX+5], curDFI;
edge edges[MAX+5];
vector<int> adj[MAX+5];
vector<int> chain;
vector<int> cyclicVertex; // first vertex in a chain which forms a cycle
bool visVertex[MAX+5], visEdge[MAX+5], isAPB[MAX+5];

void init() {
	curDFI = 0;

	memset(num, 0, sizeof num);
	memset(DFI, -1, sizeof DFI);
	memset(visVertex, 0, sizeof visVertex);
	memset(visEdge, 0, sizeof visEdge);
	memset(isAPB, 0, sizeof isAPB);

	for (int i = 0; i < N; i++) adj[i].clear();
	cyclicVertex.clear();
}

void read() {
	cin >> N >> M;

	for (int i = 0; i < M; i++) {
		int u, v; 
		cin >> u >> v;
		// u--, v--;

		edges[i] = {i, u, v, 0};

		adj[u].push_back(i);
		adj[v].push_back(i);
	}
}

void assignDFI(int u) {
	num[curDFI] = u;
	DFI[u] = curDFI++;

	for (int x : adj[u]) {
		edge & e = edges[x];
		int to = getTo(e, u);

		if (!e.directed) orient(e, to, u);
		if (DFI[to] == -1) assignDFI(to);
	}
}

void dfsPath(int u) {
	chain.push_back(u);
	if (visVertex[u]) return;

	visVertex[u] = true;
	for (int x : adj[u]) {
		edge & e = edges[x];
		if (!isUsable(e, u)) continue;

		if (DFI[u] > DFI[e.b]) {
			visEdge[e.id] = true;
			dfsPath(e.b);
		}
	}
}

void decompose(int u) {
	for (int x : adj[u]) {
		edge & e = edges[x];
		if (!isUsable(e, u)) continue;

		if (DFI[u] < DFI[e.b]) { // back edge
			chain.clear();
			chain.push_back(u);
			visVertex[u] = true;
			visEdge[e.id] = true;

			dfsPath(e.b);

			if (chain.front() == chain.back() && chain.size() >= 3) cyclicVertex.push_back(u);
		}
	}
}

int main() {
	double now = clock();

	ios_base::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);

	int TC = 1;
	for (int tc = 1; tc <= TC; tc++) {
		init();
		read();
		
		for (int i = 0; i < N; i++)
			if (DFI[i] == -1)
				assignDFI(i);

		for (int i = 0; i < N; i++) decompose(num[i]);

		cout << "Bridges:\n";
		for (int i = 0; i < M; i++)
			if (!visEdge[i]) {
				int a = edges[i].a;
				int b = edges[i].b;

				if (a > b) swap(a, b);
				cout << a << " " << b << "\n";

				if (adj[a].size() > 1) isAPB[a] = true;
				if (adj[b].size() > 1) isAPB[b] = true;
			}

		for (int i = 1; i < cyclicVertex.size(); i++) isAPB[cyclicVertex[i]] = true;

		cout << "Cut vertices:";
		for (int i = 0; i < N; i++)
			if (isAPB[i]) cout << " " << i;
		cout << "\n";
	}

	cout << "Time elapsed: " << (clock() - now) / CLOCKS_PER_SEC << " second(s)\n";
}
